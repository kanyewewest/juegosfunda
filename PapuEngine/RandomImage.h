#pragma once
#include <glm\glm.hpp>
#include "SpriteBacth.h"
#include "GLTexture.h"
#include "ResourceManager.h";

const int IMAGE_WIDTH = 60;

class RandomImage
{
protected:
	std::string textureName;
	glm::vec2 position;
public:
	RandomImage(std::string _textureName, size_t levelSize) {
		textureName = _textureName;
		position = glm::vec2(rand() % levelSize * 64, rand() % levelSize * 64);
	}

	void draw(SpriteBacth& spritebatch) {
		static Color color;
		color.r = 255;
		color.g = 255;
		color.b = 255;
		color.a = 255;
		int textureID =
			ResourceManager::getTexture(textureName).id;
		const glm::vec4 uvRect(0.0f, 0.0f, 1.0f, 1.0f);
		glm::vec4 destRect(position.x, position.y, IMAGE_WIDTH, IMAGE_WIDTH);
		spritebatch.draw(destRect, uvRect, textureID, 0.0f, color);
	}

	~RandomImage() {

	}
};

