#pragma once
#include "Agent.h"
#include "Player.h"
#include "Level.h"
#include <vector>
#include <string>
using namespace std;

class Zombie : public Agent
{
protected:
	Player * player;
	vector<string> levelData;
public:
	Zombie();
	void init(float _speed, glm::vec2 _position, Player* _player, vector<string> levelData);
	void update();
	~Zombie();
};

