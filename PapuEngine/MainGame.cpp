#include "MainGame.h"
#include "Sprite.h"
#include "ImageLoader.h"
#include <iostream>
#include "ResourceManager.h"
#include "PapuEngine.h"
#include "Zombie.h"
#include <time.h>


using namespace std;

void MainGame::run() {
	init();
	update();
}

void MainGame::init() {
	Papu::init();
	_window.create("Ella se fue :'v", _witdh, _height, 0);
	initLevel();
	initShaders();
	srand(time(NULL));
}

void MainGame::initLevel(){
	currentLevel = 0;
	levels.push_back(new Level("Levels/level.txt"));
	player = new Player();
	player->init(1.0f,
		levels[currentLevel]->getPlayerPosition(),
		&_inputManager);

	for (size_t i = 0; i < levels[currentLevel]->getZombiesPosition().size(); i++)
	{
		zombies.push_back(new Zombie());
		zombies[i]->init(0.1f,
			levels[currentLevel]->getZombiesPosition()[i], player, levels[currentLevel]->getLevelData());
	}
	
	_spriteBacth.init();
}

void MainGame::initShaders() {
	_program.compileShaders("Shaders/colorShaderVert.txt",
		"Shaders/colorShaderFrag.txt");
	_program.addAtribute("vertexPosition");
	_program.addAtribute("vertexColor");
	_program.addAtribute("vertexUV");
	_program.linkShader();
}


void MainGame::draw() {
	glClearDepth(1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	_program.use();

	glActiveTexture(GL_TEXTURE0);

	GLuint pLocation =
		_program.getUniformLocation("P");

	glm::mat4 cameraMatrix = _camera.getCameraMatrix();
	glUniformMatrix4fv(pLocation, 1,GL_FALSE, &(cameraMatrix[0][0]));

	GLuint imageLocation = _program.getUniformLocation("myImage");
	glUniform1i(imageLocation, 0);

	_spriteBacth.begin();
	levels[currentLevel]->draw();
	player->draw(_spriteBacth);
	for (size_t i = 0; i < randomImages.size(); i++)
	{
		randomImages[i]->draw(_spriteBacth);
	}
	for (size_t i = 0; i < zombies.size(); i++)
	{
		zombies[i]->draw(_spriteBacth);
	}
	_spriteBacth.end();
	_spriteBacth.renderBatch();
	
	glBindTexture(GL_TEXTURE_2D, 0);

	_program.unuse();
	_window.swapBuffer();
}

void MainGame::procesInput() {
	SDL_Event event;
	const float CAMERA_SPEED = 20.0f;
	const float SCALE_SPEED = 0.1f;
	while (SDL_PollEvent(&event))
	{
		switch (event.type)
		{
			case SDL_QUIT:
				_gameState = GameState::EXIT;
				break;
			case SDL_MOUSEMOTION:
				_inputManager.setMouseCoords(event.motion.x,event.motion.y);
			break;
			case  SDL_KEYUP:
				_inputManager.releaseKey(event.key.keysym.sym);
				switch (event.key.keysym.sym)
				{
				case SDLK_1:
					randomImages.push_back(new RandomImage("Textures/random1.png", levels[currentLevel]->getLevelSize()));
					cout << "BUTTON 1 PRESSED" << endl;
					break;
				case SDLK_2:
					randomImages.push_back(new RandomImage("Textures/random2.png", levels[currentLevel]->getLevelSize()));
					cout << "BUTTON 2 PRESSED" << endl;
					break;
				case SDLK_3:
					randomImages.push_back(new RandomImage("Textures/random3.png", levels[currentLevel]->getLevelSize()));
					cout << "BUTTON 3 PRESSED" << endl;
					break;
				case SDLK_4:
					randomImages.push_back(new RandomImage("Textures/random4.png", levels[currentLevel]->getLevelSize()));
					cout << "BUTTON 4 PRESSED" << endl;
					break;
				case SDLK_5:
					randomImages.push_back(new RandomImage("Textures/random5.png", levels[currentLevel]->getLevelSize()));
					cout << "BUTTON 5 PRESSED" << endl;
					break;
				case SDLK_6:
					randomImages.push_back(new RandomImage("Textures/random6.png", levels[currentLevel]->getLevelSize()));
					cout << "BUTTON 6 PRESSED" << endl;
					break;
				}

				break;
			case  SDL_KEYDOWN:
				_inputManager.pressKey(event.key.keysym.sym);
				break;
			case SDL_MOUSEBUTTONDOWN:
				_inputManager.pressKey(event.button.button);
				break;
			case SDL_MOUSEBUTTONUP:
				_inputManager.releaseKey(event.button.button);
				break;
		}

		/*if (_inputManager.isKeyPressed(SDLK_w)) {
			_camera.setPosition(_camera.getPosition() + glm::vec2(0.0, CAMERA_SPEED));
		}
		if (_inputManager.isKeyPressed(SDLK_s)) {
			_camera.setPosition(_camera.getPosition() - glm::vec2(0.0, CAMERA_SPEED));
		}
		if (_inputManager.isKeyPressed(SDLK_a)) {
			_camera.setPosition(_camera.getPosition() - glm::vec2(CAMERA_SPEED, 0.0));
		}
		if (_inputManager.isKeyPressed(SDLK_d)) {
			_camera.setPosition(_camera.getPosition() + glm::vec2(CAMERA_SPEED, 0.0));
		}
		if (_inputManager.isKeyPressed(SDLK_q)) {
			_camera.setScale(_camera.getScale() + SCALE_SPEED);
		}
		if (_inputManager.isKeyPressed(SDLK_e)) {
			_camera.setScale(_camera.getScale() - SCALE_SPEED);
		}
		if (_inputManager.isKeyPressed(SDL_BUTTON_LEFT)) {
			glm::vec2 mouseCoords =  _camera.convertScreenToWorl(_inputManager.getMouseCoords());
			cout << mouseCoords.x << " " << mouseCoords.y << endl;
		}*/
	}
}

void MainGame::update() {

	while (_gameState != GameState::EXIT) {
		procesInput();
		draw();
		_camera.update();
		_time += 0.002f;
		_camera.setPosition(player->getPosition());
		player->update();
		for (size_t i = 0; i < zombies.size(); i++)
		{
			zombies[i]->update();
		}
	}
}


MainGame::MainGame(): 
					  _witdh(800),
					  _height(600),
					  _gameState(GameState::PLAY),
					  _time(0)
{
	_camera.init(_witdh, _height);
}


MainGame::~MainGame()
{
}
