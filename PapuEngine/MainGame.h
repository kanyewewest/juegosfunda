#pragma once
#include <SDL\SDL.h>
#include <GL\glew.h>
#include "GLS_Program.h"
#include "Sprite.h"
#include "Window.h"
#include "Camera2D.h"
#include <vector>
#include "SpriteBacth.h"
#include "Level.h"
#include "InputManager.h"
#include "Player.h"
#include "Human.h"
#include "RandomImage.h"
#include "Zombie.h"
enum class GameState
{
	PLAY, EXIT
};


class MainGame
{
private:
	int _witdh;
	int _height;
	float _time;
	Window _window;
	void init();
	void procesInput();
	GLS_Program _program;
	Camera2D _camera;
	SpriteBacth _spriteBacth;
	InputManager _inputManager;
	vector<Level*> levels;
	vector<RandomImage*> randomImages;
	vector<Zombie*> zombies;
	int currentLevel;
	void initLevel();
	Player* player;
	vector<Human*> humans;

public:
	MainGame();
	~MainGame();
	GameState _gameState;
	void initShaders();
	void run();
	void draw();
	void update();
};

