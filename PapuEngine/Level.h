#pragma once
#include <string>
#include <vector>
#include "SpriteBacth.h"
#include "InputManager.h"
#include "RandomImage.h"
#include "Zombie.h"


using namespace std;

const int TILE_WIDTH = 64;
const int LEVEL_WIDTH = 111;

class Level
{
private:
	vector<string> levelData;
	int numHumans;
	void parseLevel();
public:
	Level(const string& filename);
	glm::vec2 playerPosition;
	vector<glm::vec2> zombiesPosition;
	glm::vec2 getPlayerPosition() const {
		return playerPosition;
	}
	vector<glm::vec2> getZombiesPosition() const {
		return zombiesPosition;
	}
	size_t getLevelSize() {
		return levelData.size();
	}
	vector<string> getLevelData() {
		return levelData;
	}
	void draw();
	SpriteBacth spritebatch;
	~Level();
};

