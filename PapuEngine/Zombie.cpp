#include "Zombie.h"
#include "Player.h"
#include "Level.h"
#include <iostream>

Zombie::Zombie()
{
}

void Zombie::init(float _speed, glm::vec2 _position, Player* _player, vector<string> _levelData)
{
	speed = _speed;
	position = _position;
	player= _player;
	levelData = _levelData;
	color.r = 0;
	color.g = 255;
	color.b = 0;
	color.a = 255;
}

void Zombie::update()
{
	glm::vec2 playerPosition = player->getPosition();

	cout << "TILE " << levelData[position.y / TILE_WIDTH][(position.x + speed) / TILE_WIDTH] << endl;
	if (playerPosition.x - position.x >= 0)  {
		if ((position.x + speed) / TILE_WIDTH > 0 && (position.x + speed)/TILE_WIDTH < LEVEL_WIDTH && levelData[position.y / TILE_WIDTH][(position.x + speed) / TILE_WIDTH] != 'B'
			&& levelData[position.y / TILE_WIDTH][(position.x + speed) / TILE_WIDTH] != 'R') {
			//cout << "X " << (position.x + speed) / TILE_WIDTH << endl;
			position.x += speed;
		}
	}
	else if ((position.x - speed) /TILE_WIDTH > 0 && (position.x - speed)/TILE_WIDTH < LEVEL_WIDTH && levelData[position.y / TILE_WIDTH][(position.x - speed) / TILE_WIDTH] != 'B'
		&& levelData[position.y / TILE_WIDTH][(position.x + speed) / TILE_WIDTH] != 'R') {
		//cout << "X " << (position.x - speed) / TILE_WIDTH << endl;
		position.x -= speed;
	}

	if (playerPosition.y - position.y >= 0) {
		if ((position.y + speed) / TILE_WIDTH > 0 && (position.y + speed) / TILE_WIDTH < LEVEL_WIDTH && levelData[(position.y + speed) / TILE_WIDTH][position.x / TILE_WIDTH] != 'B'
			&& levelData[(position.y + speed) / TILE_WIDTH][position.x / TILE_WIDTH] != 'R') {
			//cout << "Y " << (position.x + speed) / TILE_WIDTH << endl;
			position.y += speed;
		}
	}
	else if ((position.y - speed) / TILE_WIDTH > 0 && (position.y - speed) / TILE_WIDTH < LEVEL_WIDTH && levelData[(position.y - speed) / TILE_WIDTH][position.x / TILE_WIDTH] != 'B'
		&& levelData[(position.y - speed) / TILE_WIDTH][position.x / TILE_WIDTH] != 'R') {
		//cout << "Y " << (position.x - speed) / TILE_WIDTH << endl;
		position.y -= speed;
	}
}

Zombie::~Zombie()
{
}
